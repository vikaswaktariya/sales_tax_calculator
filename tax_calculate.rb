class ItemDetail
  
  def get_details
    item_details = []
    puts "Please enter the total item"
    total_item = gets.to_i
  
    (0..(total_item - 1)).each do |index|
      print "Please enter the detail of #{(index + 1)} item like 1 chocolate bar at 0.85 :\n"
      item_details[index] = gets.chomp

      if item_details[index].split(" ").last.to_f < 0
      	puts "Amount is invalid, please enter valid number"
        redo
      end

    end
    item_details
  end
  
  def print_details(item_details)
    total_amount = 0
    total_tax = 0
  
    item_details.each_with_index do |item_detail, index|
      if index == 0
       puts "Output:" 
      end	

      tax = 0
      item = item_details[index].split(" ")
      amount = calculate(item.last.to_f, item.include?("imported"), (TaxCalculator::NO_SALE_TAX_PRODUCT & item).empty?)
      puts "#{item_details[index].split(' at ').first} : #{amount}"
      total_amount += amount
      total_tax += amount - item.last.to_f
    end

    if total_tax >0 
      puts "Sales Taxes: #{ total_tax.round(2) } \n Total :#{ total_amount.round(2) }"
    end
  end

end

class TaxCalculator < ItemDetail
  NO_SALE_TAX_PRODUCT = ["chocolates", "chocolate", "headache", "book"]   
  SALE_TAX = 0.10
  IMPORT_TAX = 0.05 

  def calculate(amount, is_imported, is_apply_sales_tax)
    sales_tax  = amount * SALE_TAX  if is_apply_sales_tax
    imported_tax = amount * IMPORT_TAX if is_imported
    total_amount = (sales_tax.to_f + imported_tax.to_f + amount)
    total_amount.round(2)
  end 
end

tax = TaxCalculator.new()
tax.print_details(tax.get_details)
